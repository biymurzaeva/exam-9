import {GET_CONTACTS_FAILURE, GET_CONTACTS_REQUEST, GET_CONTACTS_SUCCESS} from "../actions/getContactsAction";

const initialState = {
	contacts: [],
	error: null,
	loading: false,
};

const getContactsReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_CONTACTS_REQUEST:
			return {...state, loading: true}
		case GET_CONTACTS_SUCCESS:
			return {...state, loading: false, contacts: action.payload}
		case GET_CONTACTS_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default getContactsReducer;