import {CURRENT_PERSON_CONTACT} from "../actions/currentPersonContactAction";

const initialState = {
	personContact: null,
};

const currentPersonContactReducer = (state= initialState, action) => {
	switch (action.type) {
		case CURRENT_PERSON_CONTACT:
			return {
				...state, personContact: action.payload
			};
		default:
			return state;
	}
}

export default currentPersonContactReducer;