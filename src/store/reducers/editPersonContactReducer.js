import {
	EDIT_PERSON_CONTACT_FAILURE,
	EDIT_PERSON_CONTACT_REQUEST,
	EDIT_PERSON_CONTACT_SUCCESS
} from "../actions/editPersonContactAction";

const initialState = {
	loading: false,
	error: null
};

const editPersonContactReducer = (state = initialState, action) => {
	switch (action.type) {
		case EDIT_PERSON_CONTACT_REQUEST:
			return {...state, loading: true}
		case EDIT_PERSON_CONTACT_SUCCESS:
			return {...state, loading: false}
		case EDIT_PERSON_CONTACT_FAILURE:
			return {...state, loading: false, error: action.payload}
		default:
			return state;
	}
};

export default editPersonContactReducer;