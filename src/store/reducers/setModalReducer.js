import {SET_MODAL_OPEN} from "../actions/setModalAction";


const initialState = {
	showPurchaseModal: false,
};

const setModalReducer = (state= initialState, action) => {
	switch (action.type) {
		case SET_MODAL_OPEN:
			return {
				...state, showPurchaseModal: action.payload
			};
		default:
			return state;
	}
}

export default setModalReducer;