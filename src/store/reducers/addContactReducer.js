import {ADD_CONTACT_FAILURE, ADD_CONTACT_REQUEST, ADD_CONTACT_SUCCESS} from "../actions/addContactAction";

const initialState = {
	loading: false,
	error: null
};

const addContactReducer = (state= initialState, action) => {
	switch (action.type) {
		case ADD_CONTACT_REQUEST:
			return {...state, loading: true}
		case ADD_CONTACT_SUCCESS:
			return {...state, loading: false}
		case ADD_CONTACT_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default addContactReducer;