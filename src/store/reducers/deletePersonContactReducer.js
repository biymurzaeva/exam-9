import {
	DELETE_PERSON_CONTACT_FAILURE,
	DELETE_PERSON_CONTACT_REQUEST,
	DELETE_PERSON_CONTACT_SUCCESS
} from "../actions/deletePersonContactAction";

export const initialState = {
	loading: false,
	error: null
};

export const deletePersonContactReducer = (state = initialState, action) => {
	switch (action.type) {
		case DELETE_PERSON_CONTACT_REQUEST:
			return {...state, loading: true}
		case DELETE_PERSON_CONTACT_SUCCESS:
			return {...state, loading: false}
		case DELETE_PERSON_CONTACT_FAILURE:
			return {...state, loading: false, error: action.payload}
		default:
			return state;
	}
};

export default deletePersonContactReducer;