import {
	GET_CURRENT_CONTACT_FAILURE,
	GET_CURRENT_CONTACT_REQUEST,
	GET_CURRENT_CONTACT_SUCCESS
} from "../actions/getCurrentContactAction";

const initialState = {
	contact: {},
	error: null,
	loading: false,
};

const getCurrentContactReducer = (state = initialState, action) => {
	switch (action.type) {
		case GET_CURRENT_CONTACT_REQUEST:
			return {...state, loading: true}
		case GET_CURRENT_CONTACT_SUCCESS:
			return {...state, loading: false, contact: action.payload}
		case GET_CURRENT_CONTACT_FAILURE:
			return {...state, error: action.payload}
		default:
			return state;
	}
};

export default getCurrentContactReducer;