export const CURRENT_PERSON_CONTACT = 'CURRENT_PERSON_CONTACT'
export const currentPersonContact = contact => ({type: CURRENT_PERSON_CONTACT, payload: contact});