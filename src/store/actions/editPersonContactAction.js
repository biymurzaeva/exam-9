import axiosApi from "../../axiosApi";

export const EDIT_PERSON_CONTACT_REQUEST = 'EDIT_PERSON_CONTACT_REQUEST';
export const EDIT_PERSON_CONTACT_SUCCESS = 'EDIT_PERSON_CONTACT_SUCCESS';
export const EDIT_PERSON_CONTACT_FAILURE = 'EDIT_PERSON_CONTACT_FAILURE';

export const editPersonContactRequest = () => ({type: EDIT_PERSON_CONTACT_REQUEST});
export const editPersonContactSuccess = () => ({type: EDIT_PERSON_CONTACT_SUCCESS});
export const editPersonContactFailure = error => ({type: EDIT_PERSON_CONTACT_FAILURE, payload: error});

export const editPersonContact = (personContactID, personContactData) => {
	return async (dispatch) => {
		try {
			dispatch(editPersonContactRequest());
			await axiosApi.put(`/contacts/${personContactID}.json`, personContactData);
			dispatch(editPersonContactSuccess());
		} catch (error) {
			dispatch(editPersonContactFailure(error));
			throw error;
		}
	}
};