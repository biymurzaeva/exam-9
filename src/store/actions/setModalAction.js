export const SET_MODAL_OPEN = 'SET_MODAL_OPEN'
export const setModalOpen = isOpen => ({type: SET_MODAL_OPEN, payload: isOpen});