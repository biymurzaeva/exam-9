import axiosApi from "../../axiosApi";

export const GET_CONTACTS_REQUEST = 'GET_CONTACTS_REQUEST';
export const GET_CONTACTS_SUCCESS = 'GET_CONTACTS_SUCCESS';
export const GET_CONTACTS_FAILURE = 'GET_CONTACTS_FAILURE';

export const getContactsRequest = () => ({type: GET_CONTACTS_REQUEST});
export const getContactsSuccess = contacts => ({type: GET_CONTACTS_SUCCESS, payload: contacts});
export const getContactsFailure = error => ({type: GET_CONTACTS_FAILURE, payload: error});

export const getContacts = () => {
	return async (dispatch) => {
		try {
			dispatch(getContactsRequest());
			const response = await axiosApi.get('/contacts.json');
			const contacts = Object.keys(response.data).map(id => ({
				...response.data[id],
				id
			}));
			dispatch(getContactsSuccess(contacts));
		} catch (error) {
			dispatch(getContactsFailure(error));
		}
	}
};