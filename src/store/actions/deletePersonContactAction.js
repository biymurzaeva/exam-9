import axiosApi from "../../axiosApi";

export const DELETE_PERSON_CONTACT_REQUEST = 'DELETE_PERSON_CONTACT_REQUEST';
export const DELETE_PERSON_CONTACT_SUCCESS = 'DELETE_PERSON_CONTACT_SUCCESS';
export const DELETE_PERSON_CONTACT_FAILURE = 'DELETE_PERSON_CONTACT_FAILURE';

export const deletePersonContactRequest = () => ({type: DELETE_PERSON_CONTACT_REQUEST});
export const deletePersonContactSuccess = () => ({type: DELETE_PERSON_CONTACT_SUCCESS});
export const deletePersonContactFailure = error => ({type: DELETE_PERSON_CONTACT_FAILURE, payload: error});

export const deletePersonContact = id => {
	return async (dispatch) => {
		try {
			dispatch(deletePersonContactRequest());
			await axiosApi.delete(`/contacts/${id}.json`);
			dispatch(deletePersonContactSuccess());
		} catch (error) {
			dispatch(deletePersonContactFailure(error));
		}
	}
};