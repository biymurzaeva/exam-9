import axiosApi from "../../axiosApi";

export const GET_CURRENT_CONTACT_REQUEST = 'GET_CURRENT_CONTACT_REQUEST';
export const GET_CURRENT_CONTACT_SUCCESS = 'GET_CURRENT_CONTACT_SUCCESS';
export const GET_CURRENT_CONTACT_FAILURE = 'GET_CONTACT_CONTACT_FAILURE';

export const getCurrentContactRequest = () => ({type: GET_CURRENT_CONTACT_REQUEST});
export const getCurrentContactSuccess = currentContact => ({type: GET_CURRENT_CONTACT_SUCCESS, payload: currentContact});
export const getCurrentContactFailure = error => ({type: GET_CURRENT_CONTACT_FAILURE, payload: error});

export const getCurrentContact = contactID => {
	return async (dispatch) => {
		try {
			dispatch(getCurrentContactRequest());
			const response = await axiosApi.get(`/contacts/${contactID}.json`);
			dispatch(getCurrentContactSuccess(response.data));
		} catch (error) {
			dispatch(getCurrentContactFailure(error));
		}
	}
};