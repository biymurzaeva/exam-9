import './App.css';
import Layout from "./UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Contacts from "./containers/Contacts/Contacts";
import AddContact from "./components/AddContact/AddContact";
import EditContact from "./components/EditContact/EditContact";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={Contacts}/>
      <Route path="/new-contact" component={AddContact}/>
      <Route path="/contacts/edit/:id" component={EditContact}/>
      <Route render={() => <h1>Not found</h1>}/>
    </Switch>
  </Layout>
);

export default App;
