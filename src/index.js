import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {createStore, applyMiddleware, combineReducers, compose} from "redux";
import thunk from "redux-thunk";
import addContactReducer from "./store/reducers/addContactReducer";
import getContactsReducer from "./store/reducers/getContactsReducer";
import setModalReducer from "./store/reducers/setModalReducer";
import currentPersonContactReducer from "./store/reducers/currentPersonContactReducer";
import deletePersonContactReducer from "./store/reducers/deletePersonContactReducer";
import getCurrentContactReducer from "./store/reducers/getCurrentContactReducer";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  addContact: addContactReducer,
  getContacts: getContactsReducer,
  setModal: setModalReducer,
  currentPersonContact: currentPersonContactReducer,
  deletePersonContact: deletePersonContactReducer,
  getCurrentContact: getCurrentContactReducer,
});

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
));

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
