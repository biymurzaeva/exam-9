import React, {useEffect} from 'react';
import './Contacts.css';
import {useDispatch, useSelector} from "react-redux";
import {getContacts} from "../../store/actions/getContactsAction";
import Contact from "../../components/Contact/Contact";
import {setModalOpen} from "../../store/actions/setModalAction";
import Modal from "../../UI/Modal/Modal";
import PersonContactData from "../../components/PersonContactData/PersonContactData";
import {currentPersonContact as contact} from "../../store/actions/currentPersonContactAction";
import {deletePersonContact} from "../../store/actions/deletePersonContactAction";
import {getCurrentContact} from "../../store/actions/getCurrentContactAction";

const Contacts = ({history}) => {
	const dispatch = useDispatch();
	const contacts = useSelector(state => state.getContacts.contacts);
	const isModalOpen = useSelector(state => state.setModal.showPurchaseModal);
	const currentPersonContact = useSelector(state => state.currentPersonContact.personContact);

	useEffect(() => {
		dispatch(getContacts());
	}, [dispatch]);

	const openModal = contactData => {
		dispatch(setModalOpen(true));
		dispatch(contact(contactData));
	};

	const closeModal = () => {
		dispatch(setModalOpen(false));
	};

	const deletePerson = async id => {
		await dispatch(deletePersonContact(id));
		await dispatch(getContacts());
		dispatch(setModalOpen(false));
	};

	const editPerson = async id => {
		await dispatch(getCurrentContact(id));
		history.push(`/contacts/edit/${id}`);
	};

	let personData;

	if (currentPersonContact) {
		personData = (
			<PersonContactData
				name={currentPersonContact.name}
				image={currentPersonContact.photo}
				phone={currentPersonContact.phone}
				email={currentPersonContact.email}
				deletePersonContact={() => deletePerson(currentPersonContact.id)}
				editPersonContact={() => editPerson(currentPersonContact.id)}
			/>
		);
	}

	return (
		<div className="Container">
			<Modal
				show={isModalOpen}
				close={closeModal}
			>
				{personData}
			</Modal>
			{contacts.map(contact => (
				<Contact
					key={contact.id}
					name={contact.name}
					image={contact.photo}
					openFullData={() => openModal(contact)}
				/>
			))}
		</div>
	);
};

export default Contacts;