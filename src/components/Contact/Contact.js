import React from 'react';
import './Contact.css';

const Contact = props => {
	return (
		<div className="Contact" onClick={props.openFullData}>
			<img src={props.image} alt={props.name} className="PersonPhoto"/>
			<h3>{props.name}</h3>
		</div>
	);
};

export default Contact;