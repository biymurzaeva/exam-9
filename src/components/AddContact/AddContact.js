import React from 'react';
import ContactForm from "../ContactForm/ContactForm";
import {useDispatch} from "react-redux";
import {addContact} from "../../store/actions/addContactAction";

const AddContact = ({history}) => {
	const dispatch = useDispatch();

	const onSubmit  = async contactData => {
		await dispatch(addContact(contactData));
		history.push('/');
	};

	return (
		<>
			<ContactForm onSubmit={onSubmit}/>
		</>
	);
};

export default AddContact;