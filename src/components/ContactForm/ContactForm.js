import React, {useState} from 'react';
import './ContactForm.css';
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {setModalOpen} from "../../store/actions/setModalAction";

const initialState = {
	name: '',
	phone: '',
	email: '',
	photo: ''
};

const ContactForm = ({newContactData, onSubmit}) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [contactData, setContactData] = useState(newContactData || initialState);

	const onInputChange = e => {
		const {name, value} = e.target;
		setContactData(prev => ({
			...prev,
			[name]: value,
		}));
	};

	const addContact = async e => {
		e.preventDefault();
		onSubmit({...contactData});
	};

	const personPhotoURL = 'https://cdn.pixabay.com/photo/2016/11/14/17/39/person-1824147_1280.png';

	const goBack = () => {
		history.push('/');
		dispatch(setModalOpen(false));
	};

	return (
		<div className="Container">
				<div className="ContactForm">
					<form onSubmit={addContact}>
						<div className="FormRow">
							<label>Name: </label>
							<input
								className="Input"
								type="text"
								name="name"
								value={contactData.name}
								onChange={onInputChange}
							/>
						</div>
						<div className="FormRow">
							<label>Phone: </label>
							<input
								className="Input"
								type="text"
								name="phone"
								value={contactData.phone}
								onChange={onInputChange}
							/>
						</div>
						<div className="FormRow">
							<label>Email: </label>
							<input
								className="Input"
								type="email"
								name="email"
								value={contactData.email}
								onChange={onInputChange}
							/>
						</div>
						<div className="FormRow">
							<label>Photo: </label>
							<input
								className="Input"
								type="text"
								name="photo"
								value={contactData.photo}
								onChange={onInputChange}
							/>
						</div>
						<div className="FormRow personPhotoBlock">
							<p className="PrevImgTitle">Photo preview:</p>
							<img src={contactData.photo || personPhotoURL } alt="Person" className="PersonPhoto"/>
						</div>
						<button type="submit" className="send">Save</button>
						<button type="button" className="back" onClick={goBack}>Back to contacts</button>
					</form>
				</div>
			</div>
	);
};

export default ContactForm;