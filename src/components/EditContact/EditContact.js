import React from 'react';
import ContactForm from "../ContactForm/ContactForm";
import {useDispatch, useSelector} from "react-redux";
import {editPersonContact} from "../../store/actions/editPersonContactAction";
import {setModalOpen} from "../../store/actions/setModalAction";

const EditContact = ({match, history}) => {
	const dispatch = useDispatch();
	const contact = useSelector(state => state.getCurrentContact.contact);

	const onSubmit = async contact => {
		await dispatch(editPersonContact(match.params.id, {...contact}));
		history.push('/');
		dispatch(setModalOpen(false));
	};

	return (
		<ContactForm
			newContactData={contact}
			onSubmit={onSubmit}
		/>
	);
};

export default EditContact;