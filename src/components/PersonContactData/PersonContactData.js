import React from 'react';
import './PersonContactData.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash, faEdit, faPhone, faEnvelope} from "@fortawesome/free-solid-svg-icons";

const PersonContactData = props => {
	return (
		<>
			<div className="PersonContactData">
				<img src={props.image} alt={props.name} className="PersonPhoto"/>
				<div className="Contacts">
					<p>{props.name}</p>
					<p><FontAwesomeIcon icon={faPhone} size="3x" className="PhoneIcon"/> {props.phone}</p>
					<p><FontAwesomeIcon icon={faEnvelope} size="3x" className="EmailIcon"/> {props.email}</p>
				</div>
			</div>
			<button onClick={props.editPersonContact} className="Edit">
				<FontAwesomeIcon icon={faEdit} size="3x" className="EditIcon"/>
				Edit
			</button>
			<button onClick={props.deletePersonContact} className="Delete">
				<FontAwesomeIcon icon={faTrash} size="3x" className="DeleteIcon"/>
				Delete
			</button>
		</>
	);
};

export default PersonContactData;