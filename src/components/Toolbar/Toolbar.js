import React from 'react';
import {NavLink} from "react-router-dom";
import './Toolbar.css';

const Toolbar = () => {
	return (
		<header className="Toolbar">
			<h3>Contacts</h3>
			<NavLink to="/new-contact" exact >Add new contact</NavLink>
		</header>
	);
};

export default Toolbar;